#if !defined(win32fe_LibFE_h)
#define win32fe_LibFE_h

#include "archiverfe.h"

namespace win32fe {
  class lib : public archiver {
  public:
    lib();
    ~lib() {}
    virtual int Parse(void);
    /* protected: */
    virtual int Archive(void);
    virtual int List(void);
    virtual int Delete(void);
    virtual int Extract(void);
    virtual int Help(void);

    virtual void FoundVerbose(win32fe::LI &);

    std::string VisualStudioDir;
    std::string VSVersion;
  };

}

#endif
