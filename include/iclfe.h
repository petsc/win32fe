#if !defined(win32fe_IclFE_h)
#define win32fe_IclFE_h

#include "clfe.h"

namespace win32fe {

  class icl : public cl {
  public:
    icl ();
    ~icl() {}
    /* protected: */
    virtual int Help(void);

    virtual int Report(const char *);

    std::string VisualCDir;
    std::string ICLVersion;
    std::string ICLLang;
    std::string regtag;
  };

  class ifl : public icl {
  public:
    ifl ();
    ~ifl() {}

    virtual void Foundl(win32fe::LI &);
    virtual void Foundm(win32fe::LI &);
  };

}

#endif

