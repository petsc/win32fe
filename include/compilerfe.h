#if !defined(win32fe_CompilerFE_h_)
#define win32fe_CompilerFE_h_

#include "toolfe.h"

namespace win32fe {

  class compiler : public tool {
  public:
    compiler();
    virtual ~compiler() {}
    virtual int Parse(void);
    virtual int Execute(void);
    /* protected: */
    virtual int Compile(void);
    virtual int Link(void);
    virtual int Help(void);
    virtual int DisplayVersion(void);

    virtual void FoundD(win32fe::LI &);
    virtual void FoundI(win32fe::LI &);
    virtual void FoundL(win32fe::LI &);
    virtual void FoundO(win32fe::LI &);
    virtual void Foundc(win32fe::LI &);
    virtual void Foundl(win32fe::LI &);
    virtual void Foundo(win32fe::LI &);
    virtual void Foundg(win32fe::LI &);
    virtual void Foundm(win32fe::LI &);

    virtual void FoundUnknown(win32fe::LI &);

    virtual void FoundHelp(win32fe::LI &);
    virtual void FoundVersion(win32fe::LI &);

    virtual bool IsAKnownTool(void);
    virtual void FixOutput(void);

    virtual std::string BuildLinkCommand(void);

    win32fe::List compilearg;
    win32fe::List linkarg;

    std::string compileoutflag;
    std::string linkoutflag;
    win32fe::LI OutputFlag;

    typedef int (win32fe::compiler::*ptm_execute)(void);
    ptm_execute doit;
  private:
    typedef void (win32fe::compiler::*ptm_parse)(win32fe::LI &);
    std::map<char,ptm_parse> Options;
  };

}

#endif
