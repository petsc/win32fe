#if !defined(win32fe_ArchiverFE_h_)
#define win32fe_ArchiverFE_h_

#include "toolfe.h"

namespace win32fe {

  class archiver : public tool {
  public:
    archiver();
    virtual ~archiver() {}
    virtual int Parse(void);
    virtual int Execute(void);
    /*  protected: */
    virtual int Archive(void);
    virtual int List(void)=0;
    virtual int Delete(void)=0;
    virtual int Extract(void)=0;
    virtual int Help(void);
    virtual int DisplayVersion(void);

    virtual void FoundFile(win32fe::LI &);
    virtual void FoundArchive(win32fe::LI &);
    virtual void FoundCmdFile(win32fe::LI &);
    virtual void FoundFlag(win32fe::LI &);

    virtual void FoundHelp(win32fe::LI &);
    virtual void FoundVersion(win32fe::LI &);

    int GetFileListFromArchive(void);
    virtual bool IsAKnownTool(void);

    win32fe::List archivearg;
    std::string   archive_name;
    bool          archive_exists;

    typedef int (win32fe::archiver::*ptm)(void);
    std::map<std::string,ptm> Options;
    ptm doit;
  };

}

#endif
