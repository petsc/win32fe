#if !defined(win32fe_DfFE_h)
#define win32fe_DfFE_h

#include "clfe.h"

namespace win32fe {

  class df : public cl {
  public:
    df ();
    ~df() {}
    /* protected: */
    virtual int Link(void);
    virtual int Help(void);
    virtual int DisplayVersion(void);

    virtual int Report(const char *);

    virtual void FoundI(win32fe::LI &);
    virtual void FoundO(win32fe::LI &);
    virtual void Foundg(win32fe::LI &);
    virtual void Foundl(win32fe::LI &);
    virtual void FoundD(win32fe::LI &);
    virtual void Foundm(win32fe::LI &);
    virtual void FoundUnknown(win32fe::LI &);

    virtual void FixOutput(void);

    std::string output_name;
    std::string VisualCDir;
    std::string cygroot;
  };

}

#endif

