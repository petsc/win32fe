# A current cygwin installation is required for rebuilding win32fe.exe and
# associated dll's.
#
# At the time of the release, the current version of the cygwin dll is 1.3.10-1

# Microsoft Visual Studio .NET is recommended for compilation of the .cpp files
# as the included STL string library is much more suitable for debugging than in
# Visual Studio 6.0.

DEFAULT: install

INSTALLDIR = bin
SRCDIR     = src
CP         = cp
MV         = mv
RM         = rm
MKDIR      = mkdir -p
CXX        = cl
CC         = cl
WIN32FEEXE = win32fe.exe
WIN32FEDLL = win32feutils.dll
WIN32FEEXEMANIFEST = win32fe.exe.manifest
WIN32FEDLLMANIFEST = win32feutils.dll.manifest

win32fe:
	@$(MAKE) -C src $(WIN32FEDLL) $(WIN32FEEXE) $(MAKEFLAGS)
install: win32fe
	@if [ ! -d "${INSTALLDIR}" ]; then \
	   echo $(MKDIR) $(INSTALLDIR); \
	   $(MKDIR) $(INSTALLDIR); \
	 fi
	@$(MAKE) movefiles
movefiles:
	$(CP) $(SRCDIR)/$(WIN32FEDLL) $(INSTALLDIR)/
	$(CP) $(SRCDIR)/$(WIN32FEEXE) $(INSTALLDIR)/
	if [ -f $(SRCDIR)/$(WIN32FEEXEMANIFEST) ]; then $(CP) $(SRCDIR)/$(WIN32FEEXEMANIFEST) $(INSTALLDIR)/; fi
	if [ -f $(SRCDIR)/$(WIN32FEDLLMANIFEST) ]; then $(CP) $(SRCDIR)/$(WIN32FEDLLMANIFEST) $(INSTALLDIR)/; fi
cleanhere:
	@touch makefile~
	$(RM) *~
cleanup: cleanhere
	@$(MAKE) -C src realclean $(MAKEFLAGS)
clean: cleanhere
	@$(MAKE) -C src clean $(MAKEFLAGS)
realclean: cleanhere
	@if [ -d bin ]; then \
	   touch bin/$(WIN32FEEXE); \
	   touch bin/$(WIN32FEDLL); \
	   touch bin/$(WIN32FEEXEMANIFEST); \
	   touch bin/$(WIN32FEDLLMANIFEST); \
	   $(RM) bin/$(WIN32FEEXE); \
	   echo ${RM} bin/$(WIN32FEEXE); \
	   $(RM) bin/$(WIN32FEDLL); \
	   echo ${RM} bin/$(WIN32FEDLL); \
	   $(RM) bin/$(WIN32FEEXEMANIFEST); \
	   echo ${RM} bin/$(WIN32FEEXEMANIFEST); \
	   $(RM) bin/$(WIN32FEDLLMANIFEST); \
	   echo ${RM} bin/$(WIN32FEDLLMANIFEST); \
	 fi
	@$(MAKE) -C src realclean $(MAKEFLAGS)

test_bootstrap:
	@if [ -f src/win32feutils.lib ]; then \
	    echo ${RM} src/win32feutils.lib; \
	    ${RM} src/win32feutils.lib; \
	fi
	$(MAKE) -C src win32fe CC="../bin/win32fe $(CC)" CXX="../bin/win32fe $(CXX)" OBJEXT=o 
clean_bootstrap:
	$(MAKE) -C src realclean OBJEXT=o

debug:
	@echo
	@echo Hopefully you aren\'t mixing debug and standard builds!
	@echo
	$(MAKE) -C src win32fe DLLFLAG=-LDd LANGFLAGS="-Z7" DEBUGFLAG=-DDEBUG
	@$(MAKE) movefiles

etags:
	@if [ -f TAGS ]; then \
	  echo ${RM} TAGS; \
	  ${RM} TAGS; \
	fi
	etags include/*.h src/*.cpp src/*.c
