#define WIN32FE_Version_Number "1.11.4"
#include "toolfe.h"
#include "festamp.h"

void win32fe::tool::SetVersion(void) {
  version_string  = "Win32 Development Tool Front End, version ";
  version_string += WIN32FE_Version_Number;
#if defined(DEBUG)
  version_string += " -- Debug --";
#endif
  version_string += " ";
  version_string += WIN32FE_Build_Date;
  return;
}
