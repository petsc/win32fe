#include "toolfe.h"

int main(int argc,char *argv[]) {
  win32fe::tool *FE = win32fe::tool::Create(argc,argv);
  int ierr = FE->Parse();
  if (!ierr) {
    ierr = FE->Execute();
  }
    
  FE->Destroy();
  return(ierr);
}
