#include <stdlib.h>
#include "clfe.h"
#include "outpipe.h"
#include "utils.h"

using namespace win32fe;

cl::cl() {
  compileoutflag = "-Fo";
  linkoutflag    = "-Fe";
  OutputFlag = compilearg.end();

  logo = false;
  VisualStudioDir="";
  VSVersion="";
}

int cl::Parse(void) {
  linkarg.push_front("-link");
  int ierr = compiler::Parse();
  if (ierr) {
    return(ierr);
  }
  if (!verbose && !logo) {
    compilearg.push_back("-nologo");
  }
  return(0);
}

int cl::Compile(void) {
  std::string cwd = win32fe::GetCurrentDirectory();
  GetShortPath(cwd);
  cwd += "\\";
  for (win32fe::LI i=file.begin();i!=file.end();i++) {
    std::string temp = *i;
    /* temp is either <shortpath>\<filename> or <filename> */
    std::string::size_type n = temp.rfind("\\");
    if (n==std::string::npos) {
      *i = cwd + temp;
    }
  }
  return(compiler::Compile());
}

int cl::Help(void) {
  compiler::Help();
  int ierr = cl::CLHelp();
  return(ierr);
}
 
int cl::CLHelp(void) {   
  std::cout << "cl specific help:" << std::endl;
  std::cout << "  win32fe uses -nologo by default for nonverbose output.  Use the flag:" << std::endl;
  std::cout << "     -logo  to disable this feature." << std::endl;
  std::cout << "  -g is identical to -Z7." << std::endl;
  std::cout << "  -O is identical to -O2." << std::endl << std::endl;
  std::cout << "=========================================================================" << std::endl << std::endl;

  std::string help = compilearg.front();
  help += " -?";

  int ierr=tool::Report(help.c_str());
  return(ierr);
}

int cl::DisplayVersion(void) {
  tool::DisplayVersion();
  std::string version=compilearg.front();

  win32fe::OutputPipe out(verbose);
  if (out.Start(version.c_str())) {
    out.GetExitCode();
    out.OutputLine();
  } else {
    /* Should there be an error message here? */
    return(1);
  }
  return(0);
}

void cl::FoundL(LI &i) {
  std::string temp = i->substr(2);
  if (temp=="D" || temp=="Dd") {
    compilearg.push_back(*i);
  } else if (win32fe::GetShortPath(temp)) {
    temp = "-libpath:"+temp;
    linkarg.push_back(temp);
  } else if (!woff) {
    std::cout << "Warning: win32fe: Library Path Not Found: ";
    std::cout << i->substr(2) << std::endl;
  }
  return;
}

void cl::Foundl(LI &i) {
  std::string temp = *i;
  if (temp=="-logo") {
    logo = true;
  } else if (temp=="-link") {
    /* No need to push -link as this is already at the front of linkargs. */
    i++;
    while(i != arg.end()) {
      /* There should probably be some options checking here, but we will pass for now. */
      linkarg.push_back(*i++);
    }
  } else {
    compiler::Foundl(i);
  }
  return;
}

void cl::Foundg(LI &i) {
  std::string temp = *i;
  if (temp=="-g") {
    compilearg.push_back("-Z7");
  } else {
    compilearg.push_back(temp);
  }
  return;
}

void cl::FoundO(LI &i) {
  std::string temp = *i;
  if (temp=="-O") {
    compilearg.push_back("-O2");
  } else {
    compilearg.push_back(temp);
  }
  return;
}

void cl::FoundUnknown(LI &i) {
  std::string temp = *i;
  if (temp=="-nologo") {
    logo = false;
  } 
  compilearg.push_back(temp);
  return;
}

int cl::Report(const char *str) {
  win32fe::OutputPipe output(verbose);
  int ierr = output.Execute(str);

  for (;;) {
    bool display=true;
    std::string outputline=output.GetLine();
    if (outputline.length()==0) break;
    /* Command line warning D4024 : unrecognized source file type 'xx.o', object file assumed */
    std::string::size_type warn=outputline.find("warning D4024");
    if (warn!=std::string::npos) {
      warn=outputline.find(".o', object file assumed");
      if (warn!=std::string::npos) {
        display = false;
      }
    }
    if (display || (verbose && !woff)) {
      std::cout << outputline;
    }
  }
  return(ierr);
}

// ---------------------------------------------------
// NVCC CUDA NVIDIA - Arthur Besen Soprano 10/10/2011
// ---------------------------------------------------

int nvcc::DisplayVersion(void) {
    tool::DisplayVersion();
    std::string version="nvcc -V";
    win32fe::OutputPipe out(verbose);
    if (out.Start(version.c_str())) {
        out.GetExitCode();
        out.OutputAll();
    } else {
        /* Should there be an error message here? */
       return(1);
    }
    return(0);
}



void nvcc::FoundUnknown(LI &i) {
  std::string temp = *i;
  std::string::size_type found;
  std::string findopt,newopt,subopt,newsubopt,iopt;
  std::string::size_type pos,lentk,len;
  std::string::size_type sfound, bsfound;

  findopt="--compiler-options=";
  found = temp.find(findopt);
  if(found != std::string::npos) {
    pos=found+findopt.length();
    subopt=temp.substr(pos,temp.length());
    // std::cout << "subopt:" << subopt << "\n";
    // split the subpots
    pos = 0;
    len = subopt.length();
    while ( pos < len) {
      // find ' ' - but skip '\ '
      bsfound = subopt.find("\\ ",pos);
      sfound = subopt.find(" ",pos);
      while (bsfound+1 == sfound) {
       bsfound = subopt.find("\\ ",sfound+1);
       sfound = subopt.find(" ",sfound+1); // note: sfound is used and changed
      }
      if (sfound == std::string::npos) lentk = len;
      else lentk = sfound-pos;

      // Finally fond the individual subopt flags. Now Check if -I and move out
      // of subopt [else keep in subopt]
      iopt = subopt.substr(pos,lentk);
      if (iopt.find("-I") == 0) {
       std::string path = iopt.substr(2,iopt.length());
       // convert "\ " to " " before getting shortpath
       while ((bsfound = path.find("\\ ",0))!=std::string::npos) path.erase(bsfound,1);
       win32fe::GetShortPath(path);
       compilearg.push_back("-I"+path);
       /* skip "-TP" option - if it exits. Othewise c++ build breaks */
      } else if (iopt.find("-TP") != 0) {
       newsubopt = newsubopt + " " + iopt;
      }
      if (sfound != std::string::npos) pos = sfound+1;
      else pos = len;
    }
    newopt =  findopt + "\"" + newsubopt +  "\"";
    compilearg.push_back(newopt);
  } else {
    compilearg.push_back(temp);
  }
  return;
}

#include <vector>
int nvcc::Link(void) {
    /* Copy all <file>.o in file list to <file>.obj */
    int i,ierr=0;
    LI f;
    /* Change these to list<string> someday */
    std::vector<std::string> ext(file.size(),""); /* File extensions */
    std::vector<std::string> temp(file.size(),"");
    for (i=0,f=file.begin();f!=file.end();i++,f++) {
        std::string::size_type n = (*f).rfind(".");
	std::string outfile = (*f).substr(0,n) + ".obj";
        ext[i] = (*f).substr(n,std::string::npos);
        if (ext[i] == ".o") {
            temp[i]=*f;
            std::string copy = "copy " + temp[i] + " " + outfile;
            if (verbose) {
                std::cout << copy << std::endl;
            }
            win32fe::CopyFile(temp[i],outfile);
            /* Replace <file>.o in the file list with <file>.obj */
            f = file.erase(f);
            f = file.insert(f,outfile);
        }
    }
    std::string link = BuildLinkCommand();
    // ADDED BY ARTHUR
    win32fe::RemoveQuotes(link);
    std::string::size_type pos = link.find("\"");
    std::string::size_type n = link.length();
    if(pos != link.npos)    link.erase(pos,1);
    // END OF ADDED BY ARTHUR
    ierr = Report(link.c_str());

    /* Remove file.obj's */
    for (i=0,f=file.begin();f!=file.end();i++,f++) {
        if (ext[i] == ".o") {
            win32fe::DeleteFile(*f,verbose);
            f = file.erase(f);
            f = file.insert(f,temp[i]);
        }
    }
    return(ierr);
}

// ---------------------------------------------------
// END OF NVCC CUDA NVIDIA - Arthur Besen Soprano 10/10/2011
// ---------------------------------------------------
