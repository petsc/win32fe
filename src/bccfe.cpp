#include <vector>
#include "bccfe.h"
#include "utils.h"

using namespace win32fe;

bcc::bcc() {
  compileoutflag = "-o";
  linkoutflag    = "-e";

  OutputFlag = compilearg.end();
}

int bcc::Parse(void) {
  if (!compiler::Parse()) {
    if (!verbose) {
      compilearg.push_back("-q");
    }
  } else {
    return(-1);
  }
  return(0);
}

int bcc::Compile(void) {
  std::string cwd = win32fe::GetCurrentDirectory();
  win32fe::GetShortPath(cwd);
  for (win32fe::LI i=file.begin();i!=file.end();i++) {
    std::string temp = *i;
    win32fe::GetPathRelative(temp,cwd);
    if (temp.substr(0,2)==".\\") {
      temp = temp.substr(2);
    }
    *i = temp;
  }
  return(compiler::Compile());
}

int bcc::Link(void) {
  if (OutputFlag==compilearg.end()) {
    std::string tempstr = linkoutflag + file.front();
    tempstr.replace(tempstr.rfind("."),std::string::npos,".exe");
    linkarg.push_front(tempstr);
  }

  int ierr = compiler::Link();
  return(ierr);
}

std::string bcc::BuildLinkCommand(void) {
  LI f = compilearg.begin();
  std::string link = *f++;
  win32fe::Merge(link,std::string::npos,compilearg,f);
  f = linkarg.begin();
  /* Note linkargs before files for bcc32 */
  win32fe::Merge(link,std::string::npos,linkarg,f);
  f = file.begin();
  win32fe::Merge(link,std::string::npos,file,f);
  return(link);
}

int bcc::Help(void) {
  compiler::Help();
  std::cout << "bcc32 specific help:" << std::endl;
  std::cout << "  Note: The bcc32 option -l conflicts with the win32fe use of -l." << std::endl;
  std::cout << "        The following additional options are enabled for bcc32." << std::endl << std::endl;
  std::cout << "  -l:<flag>    enables <flag> for the linker, ilink32.exe" << std::endl;
  std::cout << "  -l:-<flag>   disables <flag> for the linker, ilink32.exe" << std::endl;
  std::cout << "  -g is identical to -v" << std::endl << std::endl;
  std::cout << "=========================================================================" << std::endl << std::endl;
  std::string help = compilearg.front();
  
  int ierr = Report(help.c_str());
  return(ierr);
}

void bcc::Foundl(LI &i) {
  std::string temp = *i;
  if (temp[2]==':') {
    linkarg.push_back("-l" + temp.substr(3));
  } else {
    compiler::Foundl(i);
  }
  return;
} 

void bcc::Foundg(LI &i) {
  std::string temp = *i;
  if (temp=="-g") {
    compilearg.push_back("-v");
  } else {
    compilearg.push_back(temp);
  }
  return;
}
