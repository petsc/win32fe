#include <process.h>
#include "archiverfe.h"
#include "outpipe.h"
#include "utils.h"
#include <fstream>

using namespace win32fe;

archiver::archiver() {
  Options["-a"] = &archiver::Archive;
  Options["-d"] = &archiver::Delete;
  Options["-l"] = &archiver::List;
  Options["-x"] = &archiver::Extract;

  doit = &archiver::Help;

  archive_name = "";
}

int archiver::Parse(void) {
  if (!tool::Parse()) {
    LI i = arg.begin();
    archivearg.push_front(*i++);
    arg.pop_front();
    while (i !=arg.end()) {
      std::string temp = *i;
      if (temp[0]=='@') {
        FoundCmdFile(i);
      } else if (temp[0]!='-') {
        FoundFile(i);
      } else {
        std::map<std::string,archiver::ptm>::iterator it = this->Options.find(temp);
        if (it == this->Options.end()) {
          FoundFlag(i);
        } else {
          doit = it->second;
        }
      }
      i++;
      arg.pop_front();
    }
  } else {
    return(-1);
  }
  return(0);
}

int archiver::Execute(void) {
  int ierr = tool::Execute();
  if ((!ierr) && archiver::doit) {
    ierr = (this->*doit)();
  }
  return(ierr);
}

int archiver::Archive(void) {
  int ierr = 0;
  LI li = archivearg.begin();
  std::string header = *li++;
  win32fe::Merge(header,512,archivearg,li);
  li = file.begin();
  std::string archive;
  while ((li != file.end()) && (!ierr)) {
    /* Invoke archiver several times to limit arg length <512 chars */
    archive = header;
    win32fe::Merge(archive,512,file,li);
    ierr = Report(archive.c_str());
  }
  return(ierr);
}

int archiver::Help(void) {
  tool::Help();
  std::cout << "For archivers:" << std::endl;
  std::cout << "  win32fe will map the following <tool options> to their native options:" << std::endl;
  std::cout << "    ONLY ONE of the following options may be used per invokation of win32fe." << std::endl;
  std::cout << "    -a <archivename> <{object list}>" << std::endl;
  std::cout << "         Archive the list of objects." << std::endl;
  std::cout << "         The first filename listed will be taken as the archivename." << std::endl;
  std::cout << "    -d <archivename> <{object list}>" << std::endl;
  std::cout << "         Delete the list of objects from the archive." << std::endl;
  std::cout << "    -l <archivename> <destination>:" << std::endl;
  std::cout << "         List objects in the library to a file." << std::endl;
  std::cout << "         If no <destination> is specified, stdout is assumed." << std::endl;
  std::cout << "    -x <archivename> <{object list}>" << std::endl;
  std::cout << "         Extract the list of objects from the archive." << std::endl;
  std::cout << "         If no list is provided, all objects will be extracted." << std::endl;
  std::cout << "    @<cmdfilename>" << std::endl;
  std::cout << "         Specify command file [with native commands listed in file] to archiver." << std::endl;
  std::cout << "Note: Long lists of files are automatically split into appropriate lengths." << std::endl;
  std::cout << "      All lists must be space delimited." << std::endl << std::endl;
  std::cout << "============================================================================" << std::endl << std::endl;
  return(0);
}

void archiver::FoundFile(LI &i) {
  if (archive_name.length()!=0) {
    tool::FoundFile(i);
  } else {
    FoundArchive(i);
  }
  return;
}

void archiver::FoundArchive(LI &i) {
  std::string archivename;

  archivename = *i;
  win32fe::RemoveQuotes(archivename);
  /* used more as isfile() check */
  if (!win32fe::GetShortPath(archivename)) {
    archive_exists = false;
  } else {
    archive_exists = true;
  }

  /* the above check modifles archivename - so restart */
  archivename = *i;
  std::string dir = "";
  std::string::size_type n = archivename.rfind("\\");
  if (n==std::string::npos) {
    /* Not a Windows path. */
    n = archivename.rfind("/");
    if (n==std::string::npos) {
      /* Nor a cygwin path.  It must go in the current directory. */
      dir = win32fe::GetCurrentDirectory();
      win32fe::GetShortPath(dir);
      archivename = dir + "\\" + archivename;
    }
  }
  if (n!=std::string::npos) {
    /* Either a cygwin path or a Windows path. */
    /* Check if parent directory exists. */
    dir = archivename.substr(0,n);
    if (win32fe::GetShortPath(dir)) {
      archivename = dir + "\\" + archivename.substr(n+1);
    } else {
      /* Bad News.  Perhaps the tool is smart enough to handle this case?! */
      if (!woff) {
	std::cout << "Warning: win32fe: Directory not found: ";
	std::cout << archivename.substr(0,n) << std::endl;
      }
    }
  }
  archive_name = archivename;
  return;
}

void archiver::FoundCmdFile(LI &i) {
  std::string shortpath = i->substr(1);
  if (win32fe::GetShortPath(shortpath)) {
    shortpath = "@"+shortpath;
    file.push_back(shortpath);
  } else {
    if (!woff) {
      std::cout << "Warning: win32fe: Command File Not Found: " << i->substr(1) << std::endl;
    }
  }
  return;
}

void archiver::FoundFlag(LI &i) {
  std::string temp = *i;
  archivearg.push_back(*i);
  return;
}

void archiver::FoundHelp(win32fe::LI &i) {
  archiver::doit = NULL;
  tool::FoundHelp(i);
  return;
}

void archiver::FoundVersion(win32fe::LI &i) {
  archiver::doit = NULL;
  tool::FoundVersion(i);
  return;
}

int archiver::DisplayVersion(void) {
  tool::DisplayVersion();
  std::string version = archivearg.front();

  win32fe::OutputPipe out(verbose);
  if (out.Start(version.c_str())) {
    out.GetExitCode();
    out.OutputLine();
  } else {
    /* Should an error message be displayed? */
    return(1);
  }
  return(0);
}

int archiver::GetFileListFromArchive(void) {
  std::string listfile = win32fe::GetUniqueTempFileName();
  std::string listcmd = "win32fe ";
  listcmd += archivearg.front();
  listcmd += " -l " + archive_name + " " + listfile;
  { 
    win32fe::OutputPipe out(verbose);
    int ierr = out.Execute(listcmd.c_str());
    /* No output should be generated by this command, so don't send it to cout. */
    if (ierr) return(ierr);
  }
  {
    /* Create scope so that handle to listfile is released and DeleteFile succeeds. */
    std::ifstream liststr(listfile.c_str(),std::ios_base::in);
    if (!liststr) {
      std::cout << "std::ifstream create failed." << std::endl;
      return(-2);
    }
    for (;;) {
      std::string f;
      if (!std::getline(liststr,f)) break;
      file.push_back(f);
    }
  }

  win32fe::DeleteFile(listfile,verbose);
  return(0);
}
 
bool archiver::IsAKnownTool(void) {
  return(true);
}
 
