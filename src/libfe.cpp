#include "libfe.h"
#include "utils.h"

using namespace win32fe;

lib::lib() {
  doit = &archiver::Archive;
}
  
int lib::Parse(void) {
  if (!archiver::Parse()) {
    if (!verbose) {
      archivearg.push_back("-nologo");
    }
  } else {
    return(-1);
  }
  return(0);
}

int lib::Archive(void) {
  int ierr=0;
  archivearg.push_back("-out:" + archive_name);
  if (archive_exists) {
    file.push_front(archive_name);
  }
  win32fe::LI li = archivearg.begin();
  std::string header = *li++;
  win32fe::Merge(header,512,archivearg,li);
  li = file.begin();
  while ((li != file.end()) && (!ierr)) {
    std::string archive = header;
    win32fe::Merge(archive,512,file,li);
    ierr = Report(archive.c_str());
    /* On the first pass, archivearg.back == "-out:archivename" */
    /* Otherwise, archivearg.back == "archivename" */ 
    /* and "-out:..." is the arg before the back */
    if (archivearg.back()!=archive_name) { 
      archivearg.push_back(archive_name);
      win32fe::Merge(header,512,archivearg,--archivearg.end());
    }
  }
  return(ierr);
}

int lib::Delete(void) {
  int ierr = 0;
  archivearg.push_back(archive_name);
  archivearg.push_back("-remove:");
  win32fe::LI li = archivearg.begin();
  std::string header = *li++;
  win32fe::Merge(header,std::string::npos,archivearg,li);
  li = file.begin();
  while ((li!=file.end()) && (!ierr)) {
    std::string remove = header + *li++;
    ierr = Report(remove.c_str());
  }
  return(ierr);
}

int lib::List(void) {
  int ierr=0;
  if (file.size()>1) {
    std::cout << "Warning: win32fe: too many files passed with -l" << std::endl;
    return(-1);
  } else if (file.size()==1) {
    archivearg.push_back("-list:"+file.front());
  } else {
    archivearg.push_back("-list");
  }
  win32fe::LI li = archivearg.begin();
  std::string archivelist = *li++;
  win32fe::Merge(archivelist,std::string::npos,archivearg,li);
  archivelist += " " + archive_name;
  ierr = Report(archivelist.c_str());
  return(ierr);
}

int lib::Extract(void) {
  /* This is amazingly similar to lib::Delete()! */
  int ierr = 0;
  if (file.size()==0) {
    ierr = GetFileListFromArchive();
    if (ierr) {
      std::cout << "Error: win32fe: Failed to get list of files from archive." << std::endl;
      return(ierr);
    }
  }
  archivearg.push_back(archive_name);
  archivearg.push_back("-extract:");
  win32fe::LI li = archivearg.begin();
  std::string header = *li++;
  win32fe::Merge(header,std::string::npos,archivearg,li);
  li = file.begin();
  while ((li!=file.end())&&(!ierr)) {
    std::string extract = header + *li++;
    ierr = Report(extract.c_str());
  }
  return(ierr);
}

int lib::Help(void) {
  int ierr=0;
  archiver::Help();
  std::cout << "lib specific help:" << std::endl;
  std::cout << "  *   win32fe lib -a will check to see if the library exists, and will" << std::endl;
  std::cout << "      modify the arguments to lib accordingly; therefore, specifying" << std::endl;
  std::cout << "      -out:<libname> is unnecessary and may lead to unexpected results." << std::endl;
  std::cout << "  *   Use the win32fe options -l, -x, and -d instead of their" << std::endl;
  std::cout << "      lib counterparts -list:, -extract:, and -remove:, respectively." << std::endl;
  std::cout << "  Ex: win32fe lib -a libfoo.lib foo.o bar.o" << std::endl;
  std::cout << std::endl;
  std::cout << "============================================================================" << std::endl << std::endl;
  
  std::string help = archivearg.front();
  help += " -?"; 
  ierr = Report(help.c_str());
  return(ierr);
}

void lib::FoundVerbose(win32fe::LI &i) {
  archiver::FoundVerbose(i);
  archivearg.push_back("-verbose");
  return;
}

