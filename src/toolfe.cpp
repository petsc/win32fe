#include "toolfe.h"
#include "outpipe.h"
#include "utils.h"
#include <Windows.h> 
using namespace win32fe;

tool::~tool(void) {
}

tool::tool(void) {
  tool::Options["--help"] = &tool::FoundHelp;
  tool::Options["--path"] = &tool::FoundPath;
  tool::Options["--use"] = &tool::FoundUse;
  tool::Options["--verbose"] = &tool::FoundVerbose;
  tool::Options["--version"] = &tool::FoundVersion;
  tool::Options["--woff"] = &tool::FoundWoff;
  tool::Options["--NT4"] = &tool::FoundNT4;
  tool::Options["--wait_for_debugger"] = &tool::FoundWait;
  tool::Options["--win-l"] = &tool::FoundWin_l;

  woff         = false;
  verbose      = false;
  inpath       = false;
  libprefix    = "lib";
  doit         = NULL;
  waitfordebugger=0;
  SetVersion();
}
  
void tool::GetArgs(int argc,char *argv[]) {
  if (argc==1) {
    arg.push_front("--help");
  } else {
    for (int i=1;i<argc;i++) {
      std::string temp=argv[i];
      if (temp=="--wait_for_debugger") {
        waitfordebugger = 1;
        std::cout << "Waiting for debugger to attach....." << std::endl;
        std::cout << "PID = " << ::GetCurrentProcessId() << std::endl;
        std::cout << "Manually set tool::waitfordebugger to 0 once the debugger has attached." << std::endl;
        while (waitfordebugger) ::Sleep(50);
      } else if (temp=="--verbose") {
        verbose=true;
        std::cout << std::endl;
      }
      arg.push_back(temp);
    }
    if (argc == 2) {
      if (!((arg.front()=="--version") || (arg.front()=="--help"))) {
        arg.push_back("--help");
      }
    }
    if (argc == 3) {
      if (arg.back()=="--verbose") {
        arg.push_back("--help");
      }
    }
  }
  return;
}

int tool::Parse() {
  LI i = arg.begin();
  while (i!=arg.end()) {
    std::string temp = *i;
    if (temp.substr(0,2)=="--") {
      std::string flag = temp;
      std::map<std::string,tool::ptm_parse>::iterator it = this->Options.find(flag);
      if (it == this->Options.end()) {
        tool::FoundUnknown(i);
      } else {
        (this->*it->second)(i);
      }
    } else {
      i++;
    }
  }
  if (!IsAKnownTool()) {
    if (!tool::doit) {
      std::string triedtool="";
      if (arg.begin()!=arg.end()) {
        triedtool = arg.front();
      }
      std::cout << std::endl << "Error: win32fe: Unknown Tool: " << triedtool << std::endl;
      std::cout << "  Use --help for more information on win32fe options." << std::endl << std::endl;
    }
  }
  return(0);
}

int tool::Execute(void) {
  int ierr=0;
  if (doit) {
    ierr = (this->*doit)();
  } else if (verbose) {
    ierr = DisplayVersion();
  }
  return(ierr);
}

int tool::Report(const char *str) {
  int ierr = 0;
  win32fe::OutputPipe out(verbose);
  if (out.Start(str)) {
    ierr = out.GetExitCode();
    out.OutputAll();
  } else {
    return(1);
  }
  return(ierr);
}

int tool::Help(void) {
  std::cout << std::endl << version_string << std::endl << std::endl;  
  std::cout << "Usage: win32fe <tool> --<win32fe options> -<tool options> <files>" << std::endl;
  std::cout << "  <tool> must be the first argument to win32fe" << std::endl << std::endl;
  std::cout << "<tool>: {cl,icl,df,f90,ifl,bcc32,lib,tlib}" << std::endl;
  std::cout << "  cl:    Microsoft 32-bit C/C++ Optimizing Compiler" << std::endl;
  std::cout << "  icl:   Intel C/C++ Optimizing Compiler" << std::endl;
  std::cout << "  df:    Compaq Visual Fortran Optimizing Compiler" << std::endl;
  std::cout << "  f90:   Compaq Visual Fortran90 Optimizing Compiler" << std::endl;
  std::cout << "  ifl:   Intel Fortran Optimizing Compiler" << std::endl;
  std::cout << "  ifort: Intel Fortran Optimizing Compiler" << std::endl;
  std::cout << "  nvcc:  NVIDIA CUDA Compiler Driver" << std::endl;
  std::cout << "  bcc32: Borland C++ for Win32" << std::endl;
  std::cout << "  lib:   Microsoft Library Manager" << std::endl;
  std::cout << "  tlib:  Borland Library Manager" << std::endl;
  std::cout << std::endl;
  std::cout << "<win32fe options>:" << std::endl;
  std::cout << "  --help:       Output this help message and help for <tool>" << std::endl;
  std::cout << "  --path <arg>: <arg> specifies an addition to the PATH that is required" << std::endl;
  std::cout << "                (ex. the location of a required .dll)" << std::endl;
  std::cout << "  --use <arg>:  <arg> specifies the variant of <tool> to use" << std::endl;
  std::cout << "  --verbose:    Echo to stdout the translated commandline" << std::endl;
  std::cout << "                and other diagnostic information" << std::endl;
  std::cout << "  --version:    Output version info for win32fe and <tool>" << std::endl;
  std::cout << "  --wait_for_debugger:  Inserts an infinite wait after creation of <tool>" << std::endl;
  std::cout << "                and outputs PID so one can manually attach a debugger to the" << std::endl;
  std::cout << "                current process.  In the debugger, one must set:" << std::endl;
  std::cout << "                   tool::waitfordebugger = 0" << std::endl;
  std::cout << "                to continue the execution normally." << std::endl;
  std::cout << "  --win-l:      For compilers, define -lfoo to link foo.lib instead of libfoo.lib" << std::endl;
  std::cout << "  --woff:       Suppress win32fe specific warning messages" << std::endl;
  std::cout << "=================================================================================" << std::endl << std::endl;
  return(0);
}

void tool::FoundUnknown(LI &i) {
  i++;
  return;
}

void tool::FoundPath(LI &i) {
  /* Usage: --path "path1;path2;path3" */
  i = arg.erase(i);
  if (i!=arg.end()) {
    std::string inpath = *i;
    std::string::size_type nold=0;
    std::string shortpath="";
    for (;;) {
      std::string::size_type n=inpath.find(";",nold);
      std::string pathi=inpath.substr(nold,n-nold);
      if (win32fe::GetShortPath(pathi)) {
        if (verbose) {
          std::cout << "win32fe: Adding to path: " << win32fe::GetLongPath(pathi) << std::endl;
        }
        shortpath += pathi + ";";
      } else {
        if (!woff) {
          std::cout << "Warning: win32fe: Path Not Found: " << inpath.substr(nold,n-nold) << std::endl;
        }
      }
      if (n==std::string::npos) break;
      nold = n+1;
    }
    shortpath += win32fe::GetEnvironmentVariable("PATH");
    win32fe::SetEnvironmentVariable("PATH",shortpath);
    i = arg.erase(i);
  } else {
    i--;
    arg.push_back("--help");
  }
  return;
}

void tool::FoundWin_l(LI &i) {
  libprefix = "";
  i=arg.erase(i);
  if (verbose) {
    std::cout << "win32fe: Using Windows style -l for compilers, -lfoo means foo.lib not libfoo.lib" << std::endl;
  }
  return;
}

void tool::FoundUse(LI &i) {
  i = arg.erase(i);
  if (i!=arg.end()) {
    std::string tmp=*i;
    i = arg.erase(i);
    if (verbose) {
      std::cout << "Using tool: ";
      std::cout << tmp << std::endl;
    }
    win32fe::GetShortPath(tmp);
    /* If a valid path was specified, use the short form. */
    /* If a valid path was not specified, use what was provided */
    /* without warning, it's probably found in PATH. */
    arg.pop_front();
    arg.push_front(tmp);
    /* We should really be searching for tmp in PATH here! */
  } else {
    i--;
    arg.push_back("--help");
  }
  return;
}

void tool::FoundWoff(LI &i) {
  woff = true;
  i = arg.erase(i);
  return;
}

void tool::FoundNT4(LI &i) {
  if (verbose) {
    std::cout << "Warning: win32fe: --NT4 is a depricated option." << std::endl;
  }
  i = arg.erase(i);
  return;
}

void tool::FoundVerbose(LI &i) {
  verbose=true;
  i = arg.erase(i);
  return;
}

void tool::FoundVersion(LI &i) {
  doit = &tool::DisplayVersion;
  i = arg.erase(i);
  return;
}

void tool::FoundHelp(LI &i) {
  doit = &tool::Help;
  i = arg.erase(i);
  return;
}

void tool::FoundWait(LI &i) {
  i = arg.erase(i);
  return;
}

void tool::FoundFile(LI &i) {
  std::string temp = *i;
  /* If file specified with <path>\<filename> then verify it exists */
  std::string::size_type n = temp.rfind("\\");
  if (n==std::string::npos) {
    n = temp.rfind("/");
  }
  if (n!=std::string::npos) {
    std::string dir = temp.substr(0,n);
    if (win32fe::GetShortPath(dir)) {
      temp = dir + "\\" + temp.substr(n+1);
    } else {
      std::cout << "Warning: win32fe: File Not Found: " << temp << std::endl;
    }
  }
  file.push_back(temp);
  return;
}

int tool::DisplayVersion(void) {
  std::cout << version_string << std::endl;
  return(0);
}

bool tool::IsAKnownTool(void) {
  return(false);
}
