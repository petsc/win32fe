#include "outpipe_i.h"
#include <string>
#include <cstring>
#include <iostream>

extern "C" {
  void my_cygwin_conv_to_full_win32_path(const char *in,char *out) {
    std::string outstr = in;
    std::string In = in;
    if (In.find(" ")!=std::string::npos) {
      In = "\"";
      In += in;
      In += "\"";
    }
    std::string str = "cygpath -aw ";
    str += In;

    win32fe::OutputPipe output(false);
    int err=output.Execute(str.c_str());
    if (!err) {
      outstr = output.GetLine();
      if (outstr.length()==0) {
        outstr = in;
      }
      outstr = outstr.substr(0,outstr.find("\n"));
    }
    strcpy(out,outstr.c_str());
  }
}
