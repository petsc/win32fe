#include "iclfe.h"
#include "outpipe.h"
#include "utils.h"

using namespace win32fe;

icl::icl() {
  VisualCDir="";
  ICLVersion="";
  ICLLang="C++";
  regtag="Intel C/C++ Compiler for 32-bit apps, Version ";
}

int icl::Help(void) {
  compiler::Help();
  std::cout << "i"; /* This is awfully cute. */
  int ierr = cl::CLHelp();
  return(ierr);
}

int icl::Report(const char *str) {
  win32fe::OutputPipe err(verbose);
  int ierr = err.Execute(str);
  for (;;) {
    bool display=true;
    std::string line=err.GetLine();
    if (line.length()==0) break;
    std::string::size_type warn=line.find(compilearg.front()+": Command line warning:");
    if (warn!=std::string::npos) {
      warn = line.find(".o'; object file assumed");
      if (warn!=std::string::npos) {
        display = false;
      }
    }
    if (display || (verbose && !woff)) {
      std::cout << line;
    }
  }
  return(ierr);
}

ifl::ifl() {
  ICLLang = "FORTRAN";
  regtag = "Intel(R) Fortran Compiler for 32-bit apps, Version ";
}
 
void ifl::Foundl(win32fe::LI &i) {
  std::string temp=*i;
  std::string::size_type n=temp.find("-libs:");
  if (n != std::string::npos) {
    compilearg.push_back(temp);
  } else {
    icl::Foundl(i);
  }
  return;
}

void ifl::Foundm(LI &i) {
  std::string shortpath = i->substr(8);
  if (win32fe::GetShortPath(shortpath)) {
    shortpath = "-module:"+shortpath;
    compilearg.push_back(shortpath);
  } else {
    if (!woff) {
      std::cout << "Warning: ifl: module Path Not Found: " << i->substr(8) << std::endl;
    }
  }
  return;
}
