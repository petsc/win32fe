#include <string.h>
#include <stdio.h>
#include <Windows.h>
#include "utils_i.h"

void cygwin_conv_to_full_win32_path(const char *in,char *out);
void my_cygwin_conv_to_full_win32_path(const char *,char *);

/* Notes:                                                                               */
/*   All GetPath functions require caller to allocate buffers for desired paths,        */
/*      and specify the size of the output buffer with 2nd parameter, buff_size.        */
/*   Path requested is valid                      if ...   return_value > 0             */
/*   Path requested is returned in output buffer  if ...   0 < return_value < buff_size */
typedef size_t (*GetPathType)(const char *,const size_t,char *,int);
typedef DWORD (WINAPI *GetLongPathNameType)(LPCTSTR,LPTSTR,DWORD);
__declspec(dllexport) size_t win32fe_GetShortPath(const char *path,const size_t buff_size,char *shortpath,int verbose);
__declspec(dllexport) size_t win32fe_GetShortPath_nt4(const char *path,const size_t buff_size,char *shortpath,int verbose);
__declspec(dllexport) size_t win32fe_GetShortPath_default(const char *path,const size_t buff_size,char *shortpath,int verbose);
__declspec(dllexport) size_t win32fe_GetLongPath(const char *path,const size_t buff_size,char *longpath,int verbose);
__declspec(dllexport) size_t win32fe_GetLongPath_nt4(const char *path,const size_t buff_size,char *longpath,int verbose);
__declspec(dllexport) size_t win32fe_GetLongPath_default(const char *path,const size_t buff_size,char *longpath,int verbose);

__declspec(dllexport) char * win32fe_GetLastErrorMessage(void);

/* For internal use only */
size_t RemoveQuotes(const char *in,const size_t buff_size,char *out);

static GetPathType gspn=NULL;
size_t win32fe_GetShortPath(const char *longpath,const size_t buff_size,char *shortpath,int verbose) {
  char  short_path[MAX_PATH];
  DWORD buffsize;

  /* Check to see if using Windows NT4 */
  /* GetShortPathName has been reported as broken on some Windows NT4 machines. */
  if (!gspn) {
    OSVERSIONINFO osvi;
    HANDLE utilsdll;
    utilsdll=GetModuleHandle(WIN32FE_UTILS_DLL_NAME);
    if (!utilsdll) {
      if (verbose) {
        char *erm=win32fe_GetLastErrorMessage();
        printf("win32fe::GetShortPath.GetShortPath GetModuleHandle, %s, failed.\n",WIN32FE_UTILS_DLL_NAME);
        printf("%s\n",erm);
        LocalFree(erm);
      }
      return(0);
    }
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    if (!GetVersionEx(&osvi)) {
      if (verbose) {
        char *erm=win32fe_GetLastErrorMessage();
        printf("win32fe::GetShortPath.GetShortPath GetVersionEx failed.\n");
        printf("%s\n",erm);
        LocalFree(erm);
      }
      return(0);
    }
    if ((osvi.dwPlatformId==VER_PLATFORM_WIN32_NT)&&(osvi.dwMajorVersion==4)) {
      char *gspnt4="win32fe_GetShortPath_nt4";
      gspn = (GetPathType)GetProcAddress(utilsdll,gspnt4);
      if (!gspn) {
        if (verbose) {
          char *erm=win32fe_GetLastErrorMessage();
          printf("win32fe::GetShortPath.GetShortPath GetProcAddress, %s, failed.\n",gspnt4);
          printf("%s\n",erm);
          LocalFree(erm);
        }
        return(0);
      }
    } else {
      char *gspd="win32fe_GetShortPath_default";
      gspn = (GetPathType)GetProcAddress(utilsdll,gspd);
      if (!gspn) {
        if (verbose) {
          char *erm=win32fe_GetLastErrorMessage();
          printf("win32fe::GetShortPath.GetShortPath GetProcAddress, %s, failed.\n",gspd);
          printf("%s\n",erm);
          LocalFree(erm);
        }
        return(0);
      }
    }
  }
  buffsize = (*gspn)(longpath,MAX_PATH,short_path,verbose);
  if ((buffsize>0)&&(buffsize<buff_size)) {
    strncpy(shortpath,short_path,buffsize);
  }
  return((size_t)buffsize);
}

size_t win32fe_GetShortPath_nt4(const char *longpath,const size_t buff_size,char *shortpath,int verbose) {
  /* GetShortPathName is broken on Win NT4 */
  /* Also, FindFirstFile does not append the directory/drive info, */
  /* so we must search one directory at a time to get short dir names */
  /* as well as the short form of the file name.*/
  size_t valid = 1;
  char   findtemp[MAX_PATH];
  char   shorttemp[MAX_PATH];
  size_t short_len;
  char   *begin,*end;

  /* Remove any surrounding quotes. */
  RemoveQuotes(longpath,MAX_PATH,shorttemp);

  /* cygwin preserves short/long'ness of paths */
  cygwin_conv_to_full_win32_path(shorttemp,findtemp); /* cygwin.h */

  /* Initialize */
  /* cygwin sets findtemp to <Drive>:\<something> */
  /* So, we should skip the first <Drive>:\ part which FindFirstFile doesn't handle. */
  /* Check for <Drive>: just to be sure.*/ 
  short_len = strlen(findtemp);
  if (short_len>3) {
    if (findtemp[1]==':') {
      short_len = 3;
    } else {
      short_len = 0;
    }
  }
  strncpy(shorttemp,findtemp,short_len);
  shorttemp[short_len] = '\0';
  begin = findtemp + short_len;
  end   = begin;

  while(end) {
    WIN32_FIND_DATA path;              /* Windows.h */
    /* End the search string at the next "\" */
    end = strchr(begin,(int)'\\');
    if (end) {
      /* If "\" was found, then replace "\" with string termination character. */
      *end = '\0';
    }
    if (INVALID_HANDLE_VALUE != FindFirstFile(findtemp,&path)) { /* Windows.h */
      /* FindFirstFile returns the short form of the last part of the path in path.cAlternateFileName */
      /* ONLY if it is different from the long form which is in path.cFileName */
      if (path.cAlternateFileName[0]!='\0') {
        strcat(shorttemp,path.cAlternateFileName); /* Windows.h */
      } else {
        strcat(shorttemp,path.cFileName); /* Windows.h */
      }
      /* Restore the removed "\" and add one to longtemp if necessary */
      if (end) {
        strcat(shorttemp,"\\");
        *end  = '\\';
        /* Skip over this "\" for the next call to FindFirstFile */
        end++;
        begin = end;
      }
    } else {
      /* File does not exist! */
      valid = 0;
      if (verbose) {
        char *erm=win32fe_GetLastErrorMessage();
        printf("win32fe::GetShortPath.GetShortPath_nt4 FindFirstFile, %s, failed.\n",findtemp);
        printf("%s\n",erm);
        LocalFree(erm);
      }  
      break;
    }
  }

  short_len = strlen(shorttemp);
  if (valid) {
    /* Only fill the output buffer if sufficient size exists to hold the result */
    if (short_len<buff_size) {
      strncpy(shortpath,shorttemp,short_len);
      shortpath[short_len]='\0';
    }
    /* If file exists, ALWAYS return the required buffer length. */
    valid = short_len+1;
  }
  return(valid);
}

size_t win32fe_GetShortPath_default(const char *longpath,const size_t buff_size,char *shortpath,int verbose) {
  char   tmppath[MAX_PATH];    /* Windows.h */
  char   short_path[MAX_PATH]; /* Windows.h */
  DWORD  buffsize;

  RemoveQuotes(longpath,MAX_PATH,short_path);

  /* cygwin preserves short/long'ness of paths */
  cygwin_conv_to_full_win32_path(short_path,tmppath); /* cygwin.h */
  
  /* buffsize = 0 denotes failure of GetShortPathName */
  buffsize = GetShortPathName(tmppath,short_path,MAX_PATH); /* Windows.h */
  if (!buffsize) {
    if (verbose) {
      char *erm=win32fe_GetLastErrorMessage();
      printf("win32fe::GetShortPath.GetShortPath_default GetShortPathName, %s, failed.\n",tmppath);
      printf("%s\n",erm);
      LocalFree(erm);
    }  
  } else if (buffsize<buff_size) {
    /* Only fill the output buffer if sufficient size exists to hold the result */
    strncpy(shortpath,short_path,buffsize);
    shortpath[buffsize++]='\0';
  }
  /* If file exists, ALWAYS return the required buffer length. */
  return(buffsize);
}

static GetPathType glpn = NULL;
static GetLongPathNameType win32fe_GetLongPathName = NULL;
size_t win32fe_GetLongPath(const char *shortpath,const size_t buff_size,char *longpath,int verbose) {
  char  long_path[MAX_PATH]; /* Windows.h */
  DWORD buffsize;
  HANDLE utilsdll;

  if (!glpn) {
    /* Check to see if Windows GetLongPathNameA exists in Kernel32.dll. */
    /* It does not for Win95 or WinNT4 */
    char *k32dll="Kernel32.dll";
    HMODULE k32 = LoadLibrary(k32dll);
    if (!k32) {
      /* Really Bad Error!  This should never be encountered! */
      if (verbose) {
        char *erm=win32fe_GetLastErrorMessage();
        printf("win32fe::GetLongPath.GetLongPath LoadLibrary, %s, failed.\n",k32dll);
        printf("%s\n",erm);
        LocalFree(erm);
      }
      return(0);
    } else {
      /* Kernel32.dll loaded, now query for GetLongPathNameA */
      win32fe_GetLongPathName = (GetLongPathNameType)GetProcAddress(k32,"GetLongPathNameA");
      if (!FreeLibrary(k32)) {
        /* Another, Really Bad Error!  This should also never be encountered! */ 
        if (verbose) {
          char *erm=win32fe_GetLastErrorMessage();
          printf("win32fe::GetLongPath.GetLongPath FreeLibrary, %s, failed.\n",k32dll);
          printf("%s\n",erm);
          LocalFree(erm);
        }
      } 
    }
    utilsdll=GetModuleHandle(WIN32FE_UTILS_DLL_NAME);
    if (!utilsdll) {
      /* Another, Really Bad Error!  This should also never be encountered! */ 
      if (verbose) {
        char *erm=win32fe_GetLastErrorMessage();
        printf("win32fe::GetLongPath.GetLongPath GetModuleHandle, %s, failed.\n",WIN32FE_UTILS_DLL_NAME);
        printf("%s\n",erm);
        LocalFree(erm);
      }
      return(0);
    }

    if (win32fe_GetLongPathName) {
      char *glpnd="win32fe_GetLongPath_default";
      glpn = (GetPathType)GetProcAddress(utilsdll,glpnd);
      if (!glpn) {
        /* If this error is encountered, verify the string above! */
        if (verbose) {
          char *erm=win32fe_GetLastErrorMessage();
          printf("win32fe::GetLongPath.GetLongPath GetProcAddress, %s, failed.\n",glpnd);
          printf("%s\n",erm);
          LocalFree(erm);
        }
        return(0);
      }
    } else {
      char *glpnnt4="win32fe_GetLongPath_nt4";
      glpn = (GetPathType)GetProcAddress(utilsdll,glpnnt4);
      if (!glpn) {
        /* If this error is encountered, verify the string above! */
        if (verbose) {
          char *erm=win32fe_GetLastErrorMessage();
          printf("win32fe::GetLongPath.GetLongPath GetProcAddress, %s, failed.\n",glpnnt4);
          printf("%s\n",erm);
          LocalFree(erm);
        }
        return(0);
      }
    }
  }
  buffsize = (*glpn)(shortpath,MAX_PATH,long_path,verbose);

  /* Only fill the output buffer if sufficient size exists to hold the result */
  if (buffsize>0 && buffsize<buff_size) {
    strncpy(longpath,long_path,buffsize);
  }
  /* If file exists, ALWAYS return the required buffer length. */
  return((size_t)buffsize);
}

size_t win32fe_GetLongPath_nt4(const char *shortpath,const size_t buff_size,char *longpath,int verbose) {
  /* GetLongPathName does not exist on Win NT4, so we must use FindFirstFile. */
  /* FindFirstFile does not append the directory/drive info, so */
  /* we must search one directory at a time to get long dir names */
  /* as well as the long form of the file name.*/
  char   longtemp[MAX_PATH]; /* Windows.h */
  char   findtemp[MAX_PATH]; /* Windows.h */
  size_t valid  = 1;
  size_t long_len;
  char   *begin,*end;

  RemoveQuotes(shortpath,MAX_PATH,longtemp);

  /* cygwin preserves short/long'ness of paths */
  cygwin_conv_to_full_win32_path(longtemp,findtemp); /* cygwin.h */

  /* Initialize */
  /* cygwin sets findtemp to <Drive>:\<something> */
  /* So, we should skip the first <Drive>:\ part which FindFirstFile doesn't handle. */
  /* Check for <Drive>: just to be sure.*/ 
  long_len = strlen(findtemp);
  if (long_len>3) {
    if (findtemp[1]==':') {
      long_len=3;
    } else {
      long_len=0;
    }
  }
  strncpy(longtemp,findtemp,long_len);
  longtemp[long_len] = '\0';
  begin = findtemp + long_len;
  end   = begin;

  while (end) {
    WIN32_FIND_DATA path;              /* Windows.h */
    /* End the search string at the next "\" */
    end = strchr(begin,(int)'\\');
    if (end) {
      /* If "\" was found, then replace "\" with string termination character. */
      *end = '\0';
    }
    /* FindFirstFile returns the long form of the last part of the path in path.cFileName */
    if (INVALID_HANDLE_VALUE != FindFirstFile(findtemp,&path)) { /* Windows.h */
      strcat(longtemp,path.cFileName); /* Windows.h */
      /* Restore the removed "\" and add one to longtemp if necessary */
      if (end) {
        strcat(longtemp,"\\");
        *end  = '\\';
        /* Skip over this "\" for the next call to FindFirstFile */
        end++;
        begin = end;
      }
    } else {
      /* File does not exist! */
      valid = 0;
      if (verbose) {
        char *erm=win32fe_GetLastErrorMessage();
        printf("win32fe::GetLongPath.GetLongPath_nt4 FindFirstFile, %s, failed.\n",findtemp);
        printf("%s\n",erm);
        LocalFree(erm);
      }  
      break;
    }
  }

  long_len = strlen(longtemp);
  if (valid) {
    /* Only fill the output buffer if sufficient size exists to hold the result */
    if (long_len<buff_size) {
      strncpy(longpath,longtemp,long_len);
      longpath[long_len]='\0';
    }
    /* If file exists, ALWAYS return the required buffer length. */
    valid = long_len+1;
  }
  return(valid);
}

size_t win32fe_GetLongPath_default(const char *shortpath,const size_t buff_size,char *longpath,int verbose) {
  char  tmppath[MAX_PATH];    /* Windows.h */
  char  long_path[MAX_PATH]; /* Windows.h */
  DWORD buffsize;

  RemoveQuotes(shortpath,MAX_PATH,long_path);
  /* cygwin preserves short/long'ness of paths */
  cygwin_conv_to_full_win32_path(long_path,tmppath); /* cygwin.h */

  /* buffsize = 0 denotes failure of GetLongPathName */
  buffsize = win32fe_GetLongPathName(tmppath,long_path,MAX_PATH); /* Windows.h */
  if (!buffsize) {
    if (verbose) {
      char *erm=win32fe_GetLastErrorMessage();
      printf("win32fe::GetLongPath.GetLongPath_default GetLongPathName, %s, failed.\n",tmppath);
      printf("%s\n",erm);
      LocalFree(erm);
    }  
  } else if (buffsize<buff_size) {
    /* Only fill the output buffer if sufficient size exists to hold the result */
    strncpy(longpath,long_path,buffsize);
    longpath[buffsize++]='\0';
  }
  /* If file exists, ALWAYS return the required buffer length + 1 for NULL termination. */
  return((size_t)buffsize);
}

size_t RemoveQuotes(const char *in,const size_t buff_size,char *out) {
  const char *str=in;
  size_t len;

  len = strlen(in);
  if ((in[0]=='\'')||(in[0]=='\"')) {
    str++;
    len--;
  }
  if ((str[len-1]=='\'')||(str[len-1]=='\"')) {
    len--;
  }
  if (buff_size>len) {
    strncpy(out,str,len);
    out[len]='\0';
  }
  return(len);
}

char * win32fe_GetLastErrorMessage(void) {
  /* Note: The buffer returned MUST be freed by calling LocalFree(buff); */
  char *buff;
  DWORD erc     = GetLastError();
  DWORD success = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM |
                                FORMAT_MESSAGE_ALLOCATE_BUFFER |
                                FORMAT_MESSAGE_IGNORE_INSERTS,
                                NULL,
                                erc,
                                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                                (LPSTR)&buff,
                                0,
                                NULL);
  if (!success) {
    DWORD err = GetLastError();
    printf("win32fe::GetLastErrorMessage.GetLastErrorMessage FormatMessage failed with error code, %u.\n",err);
    printf("The desired error code was %u.\n",erc);
  }
  return(buff);
}

void cygwin_conv_to_full_win32_path(const char *in,char *out) {
  my_cygwin_conv_to_full_win32_path(in,out);
  return;
}
