#include "clfe.h"
#include "dffe.h"
#include "iclfe.h"
#include "bccfe.h"
#include "libfe.h"
#include "tlibfe.h"

namespace win32fe {

  tool* CreateCL(  void) {return (new cl);}
  tool* CreateICL( void) {return (new icl);}
  tool* CreateIFL( void) {return (new ifl);}
  tool* CreateDF(  void) {return (new df);}
  tool* CreateBCC( void) {return (new bcc);}
  tool* CreateLIB( void) {return (new lib);}
  tool* CreateTLIB(void) {return (new tlib);}
  tool* CreateNVCC(void) {return (new nvcc);}

  typedef tool* (*create_ptr)(void);
  typedef std::map<std::string,win32fe::create_ptr> create_map;

};

win32fe::tool* win32fe::tool::Create(int argc,char *argv[]) {

  create_map CreateTool;
  CreateTool["cl"]    = win32fe::CreateCL;
  CreateTool["icl"]   = win32fe::CreateICL;
  CreateTool["ifl"]   = win32fe::CreateIFL;
  CreateTool["ifort"] = win32fe::CreateIFL;
  CreateTool["df"]    = win32fe::CreateDF;
  CreateTool["f90"]   = win32fe::CreateDF;
  CreateTool["bcc32"] = win32fe::CreateBCC;
  CreateTool["lib"]   = win32fe::CreateLIB;
  CreateTool["tlib"]  = win32fe::CreateTLIB;
  CreateTool["nvcc"]  = win32fe::CreateNVCC;
  
  tool *Tool = NULL;
  if (argc>1) {
    create_map::iterator i = CreateTool.find(argv[1]);
    if (i!=CreateTool.end()) {
      Tool = (*i->second)();
    }
  }
  if (!Tool) {
    /* Either tool not found or argc==1 */
    Tool = new(tool);
  }
  Tool->GetArgs(argc,argv);
  return(Tool);
}

