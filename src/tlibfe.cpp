#include <Windows.h>
#include <fstream>
#include "tlibfe.h"
#include "outpipe.h"
#include "utils.h"

using namespace win32fe;

tlib::tlib() {
  archiver::Options["-u"] = &archiver::Archive;
  archiver::Options["-e"] = &archiver::Extract;
}

int tlib::Archive(void) {
  /* tlib has two flags to update an archive, -a and -u. */
  /* -a (add) does nothing if the member exists in the archive already, */
  /* while -u (update) issues a warning if the member does not exist. */
  /* We will always use -u and eliminate the warning if it is generated. */
  archivearg.push_back("/u");
  archivearg.push_back(archive_name);

  int ierr = archiver::Archive();

  /* On updates, tlib creates a backup library with .BAK */
  /* Check for existance and remove the .BAK library */
  std::string backup = archive_name;
  backup = backup.substr(1,backup.rfind(".")-1);
  backup = backup + ".BAK";
  if (win32fe::GetShortPath(backup)) {
    win32fe::DeleteFile(backup,verbose);
  }
  return(ierr);
}

int tlib::Delete(void) {
  int ierr = 0;
  archivearg.push_back(archive_name);
  archivearg.push_back("/d");
  win32fe::LI li = archivearg.begin();
  std::string header = *li++;
  win32fe::Merge(header,512,archivearg,li);
  li = file.begin();
  std::string remove;
  while ((li!=file.end()) && (!ierr)) {
    remove = header;
    win32fe::Merge(remove,512,file,li);
    ierr = Report(remove.c_str());
  }
  return(0);
}

int tlib::List(void) {
  std::string line;
  std::list<std::string>::size_type numfiles = file.size();
 
  std::string outfile;
  int ierr = 0;

  if (numfiles>1) {
    std::cout << "Warning: win32fe: too many files passed with -l" << std::endl;
    return(-1);
  }

  /* tlib generates list of symbols in the archive instead of files in the archive. */
  /* But, the symbols are grouped by file, so we can get the info we want. */
  /* Save the symbol list to a Unique Temp File. */ 
  outfile = win32fe::GetUniqueTempFileName();
  win32fe::LI li = archivearg.begin();
  std::string archivelist = *li++;
  win32fe::Merge(archivelist,std::string::npos,archivearg,li);
  archivelist += " " + archive_name + ", " + outfile;

  win32fe::OutputPipe out(verbose);
  if (out.Start(archivelist.c_str())) {
    ierr = (int)out.GetExitCode();
    /* First output line is title info, which should be absorbed, unless verbose is requested. */
    std::string title=out.GetLine();
    if (verbose) {
      std::cout << title;
    }
    for (;;) {
      /* Output any error messages */
      std::string line=out.GetLine();
      if (line.length()==0) break;
      std::cout << line;
    }
    if (ierr) return(ierr);
  } else {
    return(1);
  }
  
  {
    std::ifstream from(outfile.c_str());
    if (!from) return(1);

    HANDLE to;
    if (numfiles) {
      std::string destfile=file.front();
      win32fe::RemoveQuotes(destfile);
      to = win32fe::CreateFile(destfile,verbose);
      if (to==INVALID_HANDLE_VALUE) return(2);
    } else {
      to = win32fe::GetStdIOHandle(STD_OUTPUT_HANDLE,verbose);
    }

    while (std::getline(from,line)) {
      std::string::size_type n = line.rfind("size");
      if (n!=std::string::npos) {
        for (;;) {
          if (line[n-1]!='\t' && line[n-1]!=' ') break;
          n--;
        }
        line = line.substr(0,n);
        win32fe::WriteFile(to,line+".o\n",verbose);
      }
    }

    if (numfiles) {
      win32fe::CloseHandle(to);
    }
  }

  win32fe::DeleteFile(outfile,verbose);
  return(0);
}

int tlib::Extract(void) {
  /* This is amazingly similar to tlib::Delete()! */
  int ierr = 0;
  if (file.size()==0) {
    ierr = GetFileListFromArchive();
    if (ierr) {
      std::cout << "Error: win32fe: Failed to get list of files from archive." << std::endl;
      return(ierr);
    }
  }
  archivearg.push_back(archive_name);
  archivearg.push_back("/e");
  win32fe::LI li = archivearg.begin();
  std::string header = *li++;
  win32fe::Merge(header,512,archivearg,li);
  li = file.begin();
  std::string extract;
  while ((li!=file.end()) && (!ierr)) {
    extract = header;
    win32fe::Merge(extract,512,file,li);
    ierr = Report(extract.c_str());
  }
  return(0);
}

int tlib::Help(void) {
  archiver::Help();
  std::cout << "tlib specific help:" << std::endl;
  std::cout << "  *   win32fe requires the use of - to denote flags instead of /." << std::endl;
  std::cout << "      The translation from - to / for tlib is managed automatically." << std::endl;
  std::cout << "  *   tlib operators +, -+, *, \",\", etc. are not supported by win32fe, so" << std::endl;
  std::cout << "      use the arguments -a, -u, -e, -l, etc. accordingly." << std::endl;
  std::cout << "  *   tlib discards file extensions within archives, so win32fe assumes" << std::endl;
  std::cout << "      the extension .o with -l, -x, and -d commands." << std::endl;
  std::cout << "  Ex: win32fe tlib -P512 -C -a libfoo.lib foo.o bar.o" << std::endl;
  std::cout << std::endl;
  std::cout << "============================================================================" << std::endl << std::endl;
  std::string help = archivearg.front();

  int ierr = tool::Report(help.c_str());
  return(ierr);
}

int tlib::Report(const char *str) {
  win32fe::OutputPipe out(verbose);
  int ierr = out.Execute(str);
  std::string title=out.GetLine();
  bool showtitleonce=true;
  if (verbose) {
    /* Display Title info */
    std::cout << title;
    showtitleonce=false;
  }
  for (;;) {
    bool display=true;
    std::string line=out.GetLine();
    if (line.length()==0) break;
    /* Warning: 'xx' not found in library */ 
    std::string::size_type warn=line.find("Warning:");
    if (warn!=std::string::npos) {
      warn=line.find("not found in library");
      if (warn!=std::string::npos) {
        display = false;
      }
    }
    if (display || verbose) {
      if (showtitleonce) {
        std::cout << title;
        showtitleonce=false;
      }
      if (!woff) {
        std::cout << line;
      }
    }
  }
  return(ierr);
}

void tlib::FoundFile(win32fe::LI &i) {
  archiver::FoundFile(i);
  if (file.size()!=0) {
    std::string temp=file.back();
    file.pop_back();
    file.push_back("\"" + temp + "\"");
  } else {
    archive_name = "\"" + archive_name + "\"";
  }
  return;
}

void tlib::FoundFlag(LI &i) {
  std::string temp = *i;
  temp[0] = '/';
  archivearg.push_back(temp);
  return;
}

